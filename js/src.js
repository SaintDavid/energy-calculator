'use strict';

// programa de cálculo del gasto en luz. Todos los precios vienen en Kwh.

// Funciones a utilizar, además de lista de consumos medios en kwh de electrodomésticos.

import { consumo, kwhList, getreloj } from './helpers.js';

// Función asíncrona para la recuperación de precios a través de API.

const getPrices = async () => {
  try {
    const response = await fetch(
      `https://api.allorigins.win/get?url=https://api.preciodelaluz.org/v1/prices/all?zone=PCB`
    );

    const body = await response.json();

    // Array donde almacenamos los precios.

    const prices = JSON.parse(body.contents);
    const tprices = Object.values(prices);
    localStorage.setItem('light', JSON.stringify(prices));
  } catch (err) {
    console.error(err);
  }
};

// Cargamos datos del Api por primera vez en el localstore y programamos cada 5 minutos.
getPrices();
const priceInterval = setInterval(() => {
  getPrices();
}, 300000);

///////////////////////////////////////////////////////////////////////////////////////////

// Ponemos el reloj en funcionamiento. Actualizamos precio actual del Kwh.
// La primera vez lo ejecutamos directamente para que mustren desde principio la hora.
//
getreloj();

const timeInterval = setInterval(() => {
  //
  getreloj();
  //
}, 1000);

////////////////////////////////////////////////////////////////////////////////

// segunda opcion de html con los li directamente
let totalgasto = 0;
for (const [element, wh] of kwhList) {
  const clase = 'li.' + element;

  const li = document.querySelector(`${clase}`);
  // Si no existe en el html nos lo saltamos
  if (li === null) continue;

  // Si encuentra al artículo, le asignamos los watios que consume a la hora y calculamos
  // el consumo en euros
  const pwh = document.querySelector(`${clase}> p.consumo`);
  const pprice = document.querySelector(`${clase}> p.price`);
  //
  const arraywh = [Number(`${wh}`)];

  const precio = consumo(arraywh)[0].toFixed(4);

  pwh.textContent = `${wh}w`;
  pprice.textContent = `${precio}€`;
  //
  totalgasto += Number(precio);
}
const actualPrice = document.querySelector('p.priceAct');
actualPrice.textContent = `Total gasto: ${totalgasto.toFixed(4)}€`;
console.log(totalgasto);
