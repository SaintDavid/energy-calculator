'use strict';

// FUNCION consumo: Devuelve el importe en euros gastados en un intervalo de tiempo.

// Si manda hora inicial y hora final. Calculamos consumo en euros de cada uno de ellos entre esas horas.
// Si manda hora inicial y no hora final. Calculamos consumo en euros desde hora inicial + una hora
// Si no manda hora inicial, se coge la hora actual y se le calcula el consumo en euros en 1 hora en adelante.

// formato de las horas en HH:MM en string.

function consumo(watios, start = '', end = '') {
  //
  const light = JSON.parse(localStorage.getItem('light')) || [];
  //
  // Si nos mandan un array vacío, le devolvemos lo mismo, osea, un array vacío.
  //
  if (watios === []) return watios;

  // declaracion de variables.

  const now = new Date();
  let hourStart = 0;
  let hourEnd = 0;
  let minuteStart = 0;
  let minuteEnd = 0;

  // Si manda hora final, nos está pidiendo un intervalo, así que convertimos variables para trabajar

  if (end !== '') {
    hourEnd = Number(end.slice(0, 2));
    minuteEnd = Number(end.slice(3));
    hourStart = Number(start.slice(0, 2));
    minuteStart = Number(start.slice(3));
  }

  // Si solo manda hora inicial, convertimos y sumamos una hora a la final.

  if (start !== '' && end === '') {
    hourStart = Number(start.slice(0, 2));
    minuteStart = Number(start.slice(3));
    hourEnd = hourStart + 1;
    minuteEnd = minuteStart;
  }

  // Si no manda hora inicial, cogemos por omision la hora actual

  if (start === '') {
    hourStart = now.getHours();
    minuteStart = now.getMinutes();
    hourEnd = hourStart + 1;
    minuteEnd = minuteStart;
  }

  let arrayEuros = [];

  for (const wat of watios) {
    //
    let totalWat = 0;
    let tnextHour = '';
    let th = '';
    //
    for (let h = hourStart; h <= hourEnd; h++) {
      let nextHour = h + 1;
      //
      if (nextHour < 10) {
        tnextHour = '0' + nextHour;
      } else tnextHour = nextHour;
      //
      if (h < 10) {
        th = '0' + h;
      } else th = h;
      //
      let clave = th + '-' + tnextHour;
      let premin = light[clave].price / 1000000 / 60;
      //
      if (h === hourEnd) {
        totalWat += premin * minuteEnd * wat;
      } else {
        if (h === hourStart) {
          totalWat += premin * (60 - minuteStart) * wat;
        } else {
          totalWat += premin * 60 * wat;
        }
      }
    }
    arrayEuros.push(totalWat);
    //
  }
  return arrayEuros;
}

//////////////////////////////////////////////////////////////////////////////////////////////

//FUNCION mostcheap: Devuelve la hora y el precio más barato del día.
// La hora en formato HH-MM y el precio numérico.

function mostcheap() {
  //
  const light = JSON.parse(localStorage.getItem('light')) || [];
  //
  const arraylight = Object.values(light);

  const cheapprice = arraylight.reduce((acc, bestprice) => {
    if (bestprice.price < acc.price) acc = bestprice;
    return acc;
  });

  const phrase = `El precio más barato del día es ${
    cheapprice.price / 1000
  }€ Kwh entre las horas ${cheapprice.hour}`;

  return [cheapprice.hour, cheapprice.price / 1000, phrase];
}

//////////////////////////////////////////////////////////////////////////////////////////////

//FUNCION mostexpensive: Devuelve la hora y el precio más caro del día.
// La hora en formato HH-MM y el precio numérico.

function mostexpensive() {
  //
  const light = JSON.parse(localStorage.getItem('light')) || [];
  //
  const arraylight = Object.values(light);

  const expansiveprice = arraylight.reduce((acc, badprice) => {
    if (badprice.price > acc.price) acc = badprice;
    return acc;
  });

  const phrase = `El precio más caro del día es ${
    expansiveprice.price / 1000
  }€ Kwh entre las horas ${expansiveprice.hour}`;

  return [expansiveprice.hour, expansiveprice.price / 1000, phrase];
}

//////////////////////////////////////////////////////////////////////////////////////////////

// FUNCION currentprice: devuelve el precio de la luz en Kwh en tiempo real.
// Es decir, cuanto cuesta la luz en este momento.

function currentprice() {
  //
  const light = JSON.parse(localStorage.getItem('light')) || [];
  //
  const now = new Date();
  let hour1 = '';
  let hour2 = '';

  if (now.getHours() < 10) {
    hour1 = '0' + now.getHours();
  } else hour1 = now.getHours();

  if (now.getHours() + 1 < 10) {
    hour2 = '0' + (now.getHours() + 1);
  } else hour2 = now.getHours() + 1;

  let clave = hour1 + '-' + hour2;
  const price = light[clave].price / 1000;

  return price;
}

//////////////////////////////////////////////////////////////////////////////////////////////

// Lista de consumos por horas de los electrodomésticos en watios

const kwhList = [
  ['television', 200],
  ['nevera', 500],
  ['microondas', 800],
  ['lavavajillas', 2000],
  ['horno', 2000],
  ['vitroceramica', 1500],
  ['lavadora', 2000],
  ['secadora', 3000],
  ['aspiradora', 1200],
  ['lampara', 700],
  ['secador_de_pelo', 1700],
  ['aire_acondicionado', 1500],
  ['calefactor', 3000],
  ['cepillo_de_dientes', 3],
  ['plancha_pelo', 200],
  ['ordenador', 750],
  ['router', 4],
  ['freidora_aire', 1500],
  ['cafetera', 1000],
  ['tostadora', 800],
];

// Función que muestra la hora en pantalla.

function getreloj() {
  //
  const time = document.querySelector('p.hour');

  //
  const now = new Date();

  // obtener la hora en la configuración regional de España
  const hour = now.toLocaleTimeString('es-ES');
  time.textContent = `Hora: ${hour}`;
}

//////////////////////////////////////////////////////////////////////////////////////////////

export { consumo, mostcheap, mostexpensive, currentprice, kwhList, getreloj };
