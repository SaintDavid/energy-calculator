'use strict';

import { mostcheap, mostexpensive } from './helpers.js';

const lightPriceList = document.querySelector('ul.lightPayout');
const topHour = document.querySelector('p.betterHour');
const badHour = document.querySelector('p.worseHour');
const topRate = document.querySelector('p.betterPrice');
const badRate = document.querySelector('p.worsePrice');

let cheapHour = '';
let expensiveHour = '';
let goodPrice = '';
let badPrice = '';

[cheapHour, goodPrice] = mostcheap();

[expensiveHour, badPrice] = mostexpensive();

topHour.textContent = `${cheapHour}h`;
badHour.textContent = `${expensiveHour}h`;
topRate.textContent = `${goodPrice.toFixed(4)} € / kwh`;
badRate.textContent = `${badPrice.toFixed(4)} € / kwh`;

const light = JSON.parse(localStorage.getItem('light')) || [];

const values = Object.values(light);

const frag = document.createDocumentFragment();

for (const elements of values) {
  const li = document.createElement('li');

  li.innerHTML = `
                <article>
                    <p id="hora">${elements.hour}h</p>
                    <p id="precio">${(elements.price / 1000).toFixed(
                      4
                    )}€/ kWh.</p>
                </article>
  `;

  if (elements.hour === cheapHour) li.classList.add('good');
  if (elements.hour === expensiveHour) li.classList.add('bad');

  frag.append(li);
  lightPriceList.append(frag);
}
